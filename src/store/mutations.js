import * as types from './mutation-types'

export const mutations = {
    [types.CHECK_LOGIN](state, loginInfo) {
        state.checkLogin = loginInfo
    },
    [types.CHECK_REGISTER](state, registerInfo) {
        state.checkRegister = registerInfo
    },
    [types.GET_BOOKS](state, payload) {
        state.books = payload
    },
    [types.BOOK_DETAIL](state, bookPayload) {
        state.bookDetails = bookPayload
    },
    [types.CREATE_BOOK](state, bookPayload) {
        state.bookDetails = bookPayload
    },
    [types.GET_TYPES](state, typePayload) {
        state.types = typePayload
    },
    [types.UPDATE_BOOK](state, bookPayload) {
        state.bookDetails = bookPayload
    },
    [types.ADD_NEW_BOOK](state, payload) {
        state.addNewBook = payload
    },
    [types.DELETE_BOOK](state, bookPayload) {
        state.bookDetails = bookPayload
    },
    [types.GET_USERS](state, userPayload) {
        state.normalUsers = userPayload
    },
    [types.GET_NORMAL_USER_DETAIL](state, userPayload) {
        state.normalUserDetails = userPayload
    },
    [types.CREATE_NORMAL_USER](state, userPayload) {
        state.normalUserDetails = userPayload
    },
    [types.UPDATE_NORMAL_USER](state, userPayload) {
        state.normalUserDetails = userPayload
    },
    [types.DELETE_USER](state, userPayload) {
        state.normalUserDetails = userPayload
    },
    [types.GET_MEMBER_CARDS](state, cardPayload) {
        state.memberCards = cardPayload
    },
    [types.MEMBER_CARD_DETAIL](state, cardPayload) {
        state.memberCardDetails = cardPayload
    },
    [types.UPDATE_MEMBER_CARD](state, cardPayload) {
        state.memberCardDetails = cardPayload
    },
    [types.UPDATE_NOTIFICATION](state, payload) {
        state.notify = payload
    },
    [types.GET_TICKETS](state, ticketPayload) {
        state.tickets = ticketPayload
    },
    [types.TICKET_DETAILS] (state, ticketPayload) {
      state.ticketDetails = ticketPayload
    },
    [types.BOOK_OF_TICKETS] (state, bookTicketPayload) {
      state.booksOfTicket = bookTicketPayload
    },
    [types.GET_CONFIGS] (state, configsPayload) {
      state.configs = configsPayload
    },
    [types.CONFIG_DETAIL] (state, configDetailPayload) {
      state.configDetail = configDetailPayload
    },
    [types.GET_ADMINS](state, userPayload) {
        state.admins = userPayload
    },
    [types.ADMIN_DETAILS](state, userPayload) {
        state.adminDetails = userPayload
    },
    [types.DETAILS_OF_TICKET](state, ticketPayload) {
        state.ticketDetails = userPayload
    },
    [types.GET_BORROW_TICKETS](state, ticketPayload) {
        state.borrowTickets = ticketPayload
    }
}
