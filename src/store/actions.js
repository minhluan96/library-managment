import * as types from './mutation-types'

var headers = {
    'Access-Control-Request-Headers': '*',
    'Access-Control-Request-Method': '*',
    'Content-Type': 'application/json',
    'x-access-token': ''
}

var fetchAPI = function(url, header, commit, type) {
    return new Promise((resolve, reject) => {
        fetch(url, header)
            .then(response => {
                if (!response.ok) { throw response }
                return response.json()
            })
            .then(json => {
                commit(type, json)
                resolve(json)
            }).catch(err => {
              err.text().then(errorMessage => {
                var error = {
                  message: errorMessage,
                  status: err.status
                }
                reject(error)
              })
            })
    })
}

var getAccessToken = function () {
  var user = JSON.parse(localStorage.getItem('user'))
  if (user == null) return ''
  return user.accessToken
}

export const checkLogin = ({ commit }, loginInfo) => {
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(loginInfo)
    }

    return fetchAPI(`http://127.0.0.1:3000/users/login`, requestOptions, commit, types.CHECK_LOGIN)
}

export const checkRegister = ({ commit }, registerInfo) => {
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(registerInfo)
    }

    return fetchAPI(`http://127.0.0.1:3000/users/`, requestOptions, commit, types.CHECK_REGISTER)
}

export const getBooks = ({ commit }, pagePayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }

    return fetchAPI(`http://127.0.0.1:3000/books?page=${pagePayload.page}&per_page=${pagePayload.perPage}`, requestOptions, commit, types.GET_BOOKS)
}

export const searchBooks = ({ commit }, pagePayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/books/search?term=${pagePayload.term}&page=${pagePayload.page}&per_page=${pagePayload.perPage}`, requestOptions, commit, types.GET_BOOKS)
}

export const getBookDetails = ({ commit }, bookPayload) => {
    commit(types.BOOK_DETAIL, bookPayload)
}

export const addNewBook = ({ commit }, payload) => {
    commit(types.ADD_NEW_BOOK, payload)
}

export const createBook = ({ commit }, bookPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(bookPayload)
    }

    return fetchAPI(`http://127.0.0.1:3000/books`, requestOptions, commit, types.CREATE_BOOK)
}

export const getTypes = ({ commit }, typePayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/types/bookTypes`, requestOptions, commit, types.GET_TYPES)
}

export const deleteBook = ({ commit }, bookPayload) => {
    var requestOptions = {
        method: 'DELETE',
        headers: headers,
        body: JSON.stringify(bookPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/books/${bookPayload.ID}`, requestOptions, commit, types.DELETE_BOOK)
}

export const updateBook = ({ commit }, bookPayload) => {
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(bookPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/books/${bookPayload.ID}`, requestOptions, commit, types.UPDATE_BOOK)
}

export const getNormalUsers = ({ commit }, userPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/users/normal?page=${userPayload.page}&per_page=${userPayload.perPage}`, requestOptions, commit, types.GET_USERS)
}

export const getNormalUserDetails = ({ commit }, userPayload) => {
    commit(types.GET_NORMAL_USER_DETAIL, userPayload)
}

export const getAdmins = ({ commit }, userPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/users/admin?page=${userPayload.page}&per_page=${userPayload.perPage}`, requestOptions, commit, types.GET_ADMINS)
}

export const getAdminDetails = ({ commit }, userPayload) => {
    commit(types.ADMIN_DETAILS, userPayload)
}

export const createNormalUser = ({ commit }, userPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(userPayload)
    }
    console.log(userPayload)
    return fetchAPI(`http://127.0.0.1:3000/users?isManager=${userPayload.CapBac != null}`, requestOptions, commit, types.CREATE_NORMAL_USER)
}

export const updateNormalUser = ({ commit }, userPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(userPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/users/update/${userPayload.ID}?isManager=${userPayload.CapBac != null}`, requestOptions, commit, types.UPDATE_NORMAL_USER)
}

export const deleteUser = ({ commit }, userPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'DELETE',
        headers: headers,
        body: JSON.stringify(userPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/users/${userPayload.ID}`, requestOptions, commit, types.DELETE_USER)
}

export const getMemberCards = ({ commit }, cardPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/memberCards?page=${cardPayload.page}&per_page=${cardPayload.perPage}`, requestOptions, commit, types.GET_MEMBER_CARDS)
}

export const getMemberCardDetails = ({ commit }, cardPayload) => {
    commit(types.MEMBER_CARD_DETAIL, cardPayload)
}

export const getMemberCardTypes = ({ commit }, typePayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/types/memberCardTypes`, requestOptions, commit, types.GET_TYPES)
}

export const updateMemberCard = ({ commit }, cardPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(cardPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/memberCards/update/${cardPayload.ID}`, requestOptions, commit, types.UPDATE_MEMBER_CARD)
}

export const deleteMemberCard = ({ commit }, cardPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'DELETE',
        headers: headers,
        body: JSON.stringify(cardPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/memberCards/${cardPayload.ID}`, requestOptions, commit, types.UPDATE_MEMBER_CARD)
}

export const createMemberCard = ({ commit }, cardPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(cardPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/memberCards`, requestOptions, commit, types.MEMBER_CARD_DETAIL)
}

export const updateNotify = ({ commit }, payload) => {
    commit(types.UPDATE_NOTIFICATION, payload)
}

export const getTickets = ({ commit }, ticketPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/tickets?page=${ticketPayload.page}&per_page=${ticketPayload.perPage}`, requestOptions, commit, types.GET_TICKETS)
}

export const getTicketDetails = ({ commit }, ticketPayload) => {
    commit(types.TICKET_DETAILS, ticketPayload)
}

export const getAllBooks = ({ commit }, bookPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/books/all`, requestOptions, commit, types.GET_BOOKS)
}


export const getTicketTypes = ({ commit }, typePayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/types/ticketTypes`, requestOptions, commit, types.GET_TYPES)
}

export const createTicket = ({ commit }, ticketPayload) => {
    console.log('payload', ticketPayload)
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(ticketPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/tickets`, requestOptions, commit, types.TICKET_DETAILS)
}

export const validateTicket = ({ commit }, ticketPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(ticketPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/tickets/validate`, requestOptions, commit, types.TICKET_DETAILS)
}

export const getBooksOfTicket = ({ commit }, bookTicketPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/tickets/${bookTicketPayload.ID}/books`, requestOptions, commit, types.BOOK_OF_TICKETS)
}

export const updateTicket = ({ commit }, ticketPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(ticketPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/tickets/${ticketPayload.ID}`, requestOptions, commit, types.TICKET_DETAILS)
}

export const getDetailsOfTicket = ({ commit }, ticketPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/tickets/details/${ticketPayload.ID}`, requestOptions, commit, types.DETAILS_OF_TICKET)
}

export const deleteTicket = ({ commit }, ticketPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'DELETE',
        headers: headers,
        body: JSON.stringify(ticketPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/tickets/${ticketPayload.ID}`, requestOptions, commit, types.TICKET_DETAILS)
}

export const getAllConfigs = ({ commit }, configsPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/config/`, requestOptions, commit, types.GET_CONFIGS)
}

export const getConfigDetails = ({ commit }, configDetailPayload) => {
    commit(types.CONFIG_DETAIL, configDetailPayload)
}

export const updateConfig = ({ commit }, configDetailPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(configDetailPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/config/${configDetailPayload.ID}`, requestOptions, commit, types.CONFIG_DETAIL)
}

export const deleteConfig = ({ commit }, configDetailPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'DELETE',
        headers: headers,
        body: JSON.stringify(configDetailPayload)
    }
    return fetchAPI(`http://127.0.0.1:3000/config/${configDetailPayload.ID}`, requestOptions, commit, types.CONFIG_DETAIL)
}

export const searchTickets = ({ commit }, pagePayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/tickets/search?term=${pagePayload.term}&page=${pagePayload.page}&per_page=${pagePayload.perPage}`, requestOptions, commit, types.GET_TICKETS)
}

export const getConfigsOfUser = ({ commit }, configsPayload) => {
    headers['x-access-token'] = getAccessToken()
    var requestOptions = {
        method: 'GET',
        headers: headers
    }
    return fetchAPI(`http://127.0.0.1:3000/config/user`, requestOptions, commit, types.GET_CONFIGS)
}

export const getTotalUser = ({commit}, userPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/users/total`, requestOptions, commit, types.GET_USERS)
}

export const getTotalBooks = ({commit}, bookPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/books/total`, requestOptions, commit, types.GET_BOOKS)
}

export const getTotalMemberCards = ({commit}, memberCardPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/memberCards/total`, requestOptions, commit, types.GET_MEMBER_CARDS)
}

export const getTotalTickets = ({commit}, ticketPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/tickets/total`, requestOptions, commit, types.GET_TICKETS)
}

export const getReportOfTickets = ({commit}, ticketPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/tickets/report`, requestOptions, commit, types.GET_TICKETS)
}

export const getReportOfBooks = ({commit}, bookPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/books/report`, requestOptions, commit, types.GET_BOOKS)
}

export const getBorrowTicketsReport = ({commit}, ticketPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/tickets/borrowTickets`, requestOptions, commit, types.GET_TICKETS)
}

export const getBorrowTickets = ({commit}, ticketPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/tickets/borrowTickets`, requestOptions, commit, types.GET_BORROW_TICKETS)
}

export const getBorrowBooksReport = ({commit}, bookPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/books/reportStatusBooks`, requestOptions, commit, types.GET_BOOKS)
}

export const getAllNormalUserIDs = ({commit}, userPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/users/totalNormalID`, requestOptions, commit, types.GET_USERS)
}

export const getActiveNormalUserIDs = ({commit}, userPayload) => {
  headers['x-access-token'] = getAccessToken()
  var requestOptions = {
      method: 'GET',
      headers: headers
  }
  return fetchAPI(`http://127.0.0.1:3000/users/activeNormalUserID`, requestOptions, commit, types.GET_USERS)
}
