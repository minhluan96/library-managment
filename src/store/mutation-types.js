//login
export const CHECK_LOGIN = 'CHECK_LOGIN'

//register
export const CHECK_REGISTER = 'CHECK_REGISTER'

// books
export const GET_BOOKS = 'GET_BOOKS'
export const BOOK_DETAIL = 'BOOK_DETAIL'
export const CREATE_BOOK = 'CREATE_BOOK'
export const GET_TYPES = 'GET_TYPES'
export const UPDATE_BOOK = 'UPDATE_BOOK'
export const ADD_NEW_BOOK = 'ADD_NEW_BOOK'
export const DELETE_BOOK = 'DELETE_BOOK'

// normal user
export const GET_USERS = 'GET_USERS'
export const GET_NORMAL_USER_DETAIL = 'GET_NORMAL_USER_DETAIL'
export const CREATE_NORMAL_USER = 'CREATE_NORMAL_USER'
export const UPDATE_NORMAL_USER = 'UPDATE_NORMAL_USER'
export const DELETE_USER = 'DELETE_USER'

// member Cards
export const GET_MEMBER_CARDS = 'GET_MEMBER_CARDS'
export const MEMBER_CARD_DETAIL = 'MEMBER_CARD_DETAIL'
export const UPDATE_MEMBER_CARD = 'UPDATE_MEMBER_CARD'

// notification
export const UPDATE_NOTIFICATION = 'UPDATE_NOTIFICATION'

// response
export const RESPONSE = 'RESPONSE'
export const ERROR = 'ERROR'

// tickets
export const GET_TICKETS = 'GET_TICKETS'
export const GET_BORROW_TICKETS = 'GET_BORROW_TICKETS'
export const TICKET_DETAILS = 'TICKET_DETAILS'
export const BOOK_OF_TICKETS = 'BOOK_OF_TICKETS'
export const DETAILS_OF_TICKET = 'DETAILS_OF_TICKET'

// configs
export const GET_CONFIGS = 'GET_CONFIGS'
export const CONFIG_DETAIL = 'CONFIG_DETAIL'

// admins
export const GET_ADMINS = 'GET_ADMINS'
export const ADMIN_DETAILS = 'ADMIN_DETAILS'
