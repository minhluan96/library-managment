import Vue from 'vue'
import Vuex from 'vuex'
import { mutations } from './mutations'
import * as actions from './actions'

Vue.use(Vuex)

const state = {
    books: {},
    bookDetails: {},
    types: {},
    addNewBook: {},
    checkLogin: {},
    checkRegister: {},
    normalUsers: {},
    normalUserDetails: {},
    memberCards: {},
    memberCardDetails: {},
    notify: {},
    tickets: {},
    ticketDetails: {},
    booksOfTicket: {},
    configs: {},
    configDetail: {},
    admins: {},
    adminDetails: {},
    borrowTickets: {}
}

export default new Vuex.Store({
    state,
    mutations,
    actions
})
