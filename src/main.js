import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import * as uiv from 'uiv'
import Vuelidate from 'vuelidate'
import VeeValidate from "vee-validate";
import { Validator } from 'vee-validate';

// LightBootstrap plugin
import LightBootstrap from './light-bootstrap-main'

// router setup
import routes from './routes/routes'
// plugin setup
Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(uiv)
Vue.use(LightBootstrap)
Vue.use(BootstrapVue)
Vue.use(Vuelidate)
Vue.use(VeeValidate);


const dictionary = {
    // en: {
    //     messages: {
    //         required: () => 'Some English Message'
    //     }
    // },
    vi: {
        messages: {
            required: 'Vui lòng nhập trường này',
            min: 'Mật khẩu tối thiểu 6 ký tự'
        }
    }
};

// Override and merge the dictionaries
Validator.localize(dictionary);
const validator = new Validator({ username: 'required, min' });
validator.localize('vi');

// configure router
const router = new VueRouter({
    routes, // short for routes: routes
    linkActiveClass: 'nav-item active',
    mode: 'history'
})

var getAccessToken = function() {
    var user = JSON.parse(localStorage.getItem('user'))
    if (user == null) return ''
    return user.accessToken
}

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!getAccessToken()) {
            next({
                path: '/',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next()
    }
})

/* eslint-disable no-new */
new Vue({
    el: '#app',
    render: h => h(App),
    store,
    router
})