import DashboardLayout from "../components/Dashboard/Layout/DashboardLayout.vue";
import LoginForm from "../components/Dashboard/Layout/LoginForm.vue";
import RegisterForm from "../components/Dashboard/Layout/RegisterForm.vue";
// GeneralViews
import NotFound from "../components/GeneralViews/NotFoundPage.vue";

// Admin pages
import Overview from "src/components/Dashboard/Views/Overview.vue";
import UserProfile from "src/components/Dashboard/Views/UserProfile.vue";
import MemberCard from 'src/components/Dashboard/Views/MemberCard.vue'
import Books from "src/components/Dashboard/Views/Books.vue";
import Tickets from "src/components/Dashboard/Views/Tickets.vue";
import Icons from "src/components/Dashboard/Views/Icons.vue";
import Configure from "src/components/Dashboard/Views/Configure.vue";
import Notifications from "src/components/Dashboard/Views/Notifications.vue";
import Librarian from "src/components/Dashboard/Views/Librarian.vue";

const routes = [
    // {
    //   path: '/',
    //   component: DashboardLayout,
    //   redirect: '/admin/overview'
    // },
    // {
    //   path: '/login',
    //   component: LoginForm,
    //   redirect: '/login'
    // },
    {
        path: '/',
        //path: '/login',
        name: 'loginform',
        component: LoginForm
    },
    {
        path: '/register',
        name: 'registerform',
        component: RegisterForm,
        //redirect: '/register'
    },
    {
        path: '/admin',
        component: DashboardLayout,
        redirect: '/admin/overview',
        meta: {
          requiresAuth: true
        },
        children: [{
                path: 'overview',
                name: 'Overview',
                component: Overview
            },
            {
                path: 'user',
                name: 'User',
                component: UserProfile
            },
            {
                path: 'memberCard',
                name: 'Member Card',
                component: MemberCard
            },
            {
                path: 'books',
                name: 'Books',
                component: Books
            },
            {
                path: 'tickets',
                name: 'Tickets',
                component: Tickets
            },
            {
                path: 'icons',
                name: 'Icons',
                component: Icons
            },
            {
                path: 'librarian',
                name: 'Librarian',
                component: Librarian
            },
            {
                path: 'configure',
                name: 'Configure',
                component: Configure
            },
            {
                path: 'notifications',
                name: 'Notifications',
                component: Notifications
            }
        ]
    },
    { path: '*', component: NotFound }
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
